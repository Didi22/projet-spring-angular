import { HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Module } from 'src/interfaces/module.interface';
import { ModuleService } from '../module.service';

@Component({
  selector: 'post-module',
  templateUrl: './post-module.component.html',
  styleUrls: ['./post-module.component.css']
})
export class PostModuleComponent implements OnInit {

  module: Module = {
    id: 0,
    libelle: ""
  }
  constructor(private moduleService: ModuleService) { }

  ngOnInit(): void {
  }

  handleSubmit(){
    this.moduleService
    .postModule(this.module)
    .subscribe((res: HttpResponse<Module>) => {
      console.log(res);
    }
    );  
  }

}
