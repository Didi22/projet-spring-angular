import { HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Module } from 'src/interfaces/module.interface';
import { ModuleService } from '../module.service';

@Component({
  selector: 'delete-module',
  templateUrl: './delete-module.component.html',
  styleUrls: ['./delete-module.component.css']
})
export class DeleteModuleComponent implements OnInit {

  module: Module = {
    id: 0,
    libelle: ""
  }

  constructor(private moduleService: ModuleService) { }

  ngOnInit(): void {
  }

  handleSubmit(){
    this.moduleService
    .deleteModule(this.module)
    .subscribe((res: HttpResponse<Module>) => {
      console.log(res);
    }
    );  
  }
}
