import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FindAllModuleComponent } from './find-all-module/find-all-module.component';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { DeleteModuleComponent } from './delete-module/delete-module.component';
import { UpdateModuleComponent } from './update-module/update-module.component';
import { PostModuleComponent } from './post-module/post-module.component';
import { ModuleCardComponent } from './module-card/module-card.component';

const routes: Routes = [
  {path: 'module/affichage', component : FindAllModuleComponent},
  {path: 'module/affichage/:id', component : ModuleCardComponent},
  {path: 'module/enregistrement', component : PostModuleComponent},
  {path: 'module/modification', component : UpdateModuleComponent},
  {path: 'module/suppression', component : DeleteModuleComponent}
];


@NgModule({
  declarations: [
    FindAllModuleComponent,
    DeleteModuleComponent,
    UpdateModuleComponent,
    PostModuleComponent,
    ModuleCardComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forRoot(routes)
  ],
  exports: [ModuleCardComponent, PostModuleComponent, UpdateModuleComponent, DeleteModuleComponent, RouterModule]
})
export class ModuleModule { }
