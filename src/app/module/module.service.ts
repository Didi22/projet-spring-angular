import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Module } from 'src/interfaces/module.interface';

const API = "http://localhost:8080/api/module/";

@Injectable({
  providedIn: 'root'
})
export class ModuleService {

  constructor(private http: HttpClient) { }

  getModules(): Observable<Module[]> {
    return this.http.get<Module[]>(API);  
  }

  getByIdModule(module: Module): Observable<Module> {
    return this.http.get<Module>(API + '/' + module.id);  
  }

  postModule(module: Module): Observable<HttpResponse<Module>> {
    const headers: HttpHeaders = new HttpHeaders();
    return this.http.post<Module>(API, module, { headers, observe: 'response' });
  }

  updateModule(module: Module): Observable<HttpResponse<Module>> {
    const headers: HttpHeaders = new HttpHeaders();
    return this.http.put<Module>(API, module, { headers, observe: 'response' });
  }

  deleteModule(module: Module): Observable<HttpResponse<Module>> {
    const headers: HttpHeaders = new HttpHeaders();
    return this.http.delete<Module>(API + "/" + module.id, { headers, observe: 'response' });
  }

}
