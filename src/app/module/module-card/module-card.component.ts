import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Module } from 'src/interfaces/module.interface';
import { ModuleService } from '../module.service';

@Component({
  selector: 'module-card',
  templateUrl: './module-card.component.html',
  styleUrls: ['./module-card.component.css']
})
export class ModuleCardComponent implements OnInit {

  urlParam: string | null = '';

  moduleSelected: Module = {
    id: 0,
    libelle: ""
  }
  constructor(private moduleService: ModuleService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.urlParam = this.route.snapshot.paramMap.get('id');
    this.moduleSelected.id = Number(this.urlParam);
    
    this.moduleService.getByIdModule(this.moduleSelected)
    .subscribe((res: any) => this.moduleSelected = res);
  }

}
