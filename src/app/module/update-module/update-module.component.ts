import { HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Module } from 'src/interfaces/module.interface';
import { ModuleService } from '../module.service';

@Component({
  selector: 'update-module',
  templateUrl: './update-module.component.html',
  styleUrls: ['./update-module.component.css']
})
export class UpdateModuleComponent implements OnInit {

  module: Module = {
    id: 0,
    dateDebut: new Date(),
    dateFin: new Date(),
    libelle: "",
    filiere_id: 0,
    personne_id: 0
  }

  constructor(private moduleService: ModuleService) { }

  ngOnInit(): void {
  }

  handleSubmit(){
    this.moduleService
    .postModule(this.module)
    .subscribe((res: HttpResponse<Module>) => {
      console.log(res);
    }
    );  
  }

}
