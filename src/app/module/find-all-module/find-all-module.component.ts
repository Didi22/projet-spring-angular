import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs';
import { Module } from 'src/interfaces/module.interface';
import { ModuleService } from '../module.service';

@Component({
  selector: 'find-all-module',
  templateUrl: './find-all-module.component.html',
  styleUrls: ['./find-all-module.component.css']
})
export class FindAllModuleComponent implements OnInit {

  modules: Module[] = [];

  module: Module = {
    id: 0,
    libelle: ""
  }

  urlParam: string | null = '';

  constructor(private moduleService: ModuleService, private route: ActivatedRoute) { 
    const source$ = this.moduleService.getModules();

    source$.pipe(
      switchMap((modules: Module[]) => modules)
    ).subscribe((module: any) => {
      this.modules.push(module);
    } );
  }

  ngOnInit(): void {
    this.urlParam = this.route.snapshot.paramMap.get('id');
  }

  handleSubmit(){
    this.moduleService
    .getByIdModule(this.module)
    .subscribe((res: Module) => {
      this.module = res;
    }
    );  
  }


}
