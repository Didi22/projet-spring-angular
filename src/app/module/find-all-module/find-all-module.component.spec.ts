import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FindAllModuleComponent } from './find-all-module.component';

describe('FindAllModuleComponent', () => {
  let component: FindAllModuleComponent;
  let fixture: ComponentFixture<FindAllModuleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FindAllModuleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FindAllModuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
