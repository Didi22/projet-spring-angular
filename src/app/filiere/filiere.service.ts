import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Filiere } from 'src/interfaces/filiere.interface';
import { Module } from 'src/interfaces/module.interface';

const API = "http://localhost:8080/api/filiere";
const API2 = "http://localhost:8080/api/module/filiere";

@Injectable({
  providedIn: 'root'
})
export class FiliereService {

  constructor(private http: HttpClient) { }

  getFilieresAsync(): Observable<Filiere[]> {
    return this.http.get<Filiere[]>(API);  
  }

  getByIdFiliere(filiere: Filiere): Observable<Filiere> {
    return this.http.get<Filiere>(API + '/' + filiere.id);  
  }

  postFiliere(filiere: Filiere): Observable<HttpResponse<Filiere>> {
    const headers: HttpHeaders = new HttpHeaders();
    return this.http.post<Filiere>(API, filiere, { headers, observe: 'response' });
  }

  updateFiliere(filiere: Filiere): Observable<HttpResponse<Filiere>> {
    const headers: HttpHeaders = new HttpHeaders();
    return this.http.put<Filiere>(API, filiere, { headers, observe: 'response' });
  }

  deleteFiliere(filiere: Filiere): Observable<HttpResponse<Filiere>> {
    const headers: HttpHeaders = new HttpHeaders();
    return this.http.delete<Filiere>(API + "/" + filiere.id, { headers, observe: 'response' });
  }

  getModulesOfFiliere(filiere: Filiere): Observable<Module[]> {
    return this.http.get<Module[]>(API2 + '/' + filiere.id);  
  }
}
