import { HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Filiere } from 'src/interfaces/filiere.interface';
import { FiliereService } from '../filiere.service';

@Component({
  selector: 'delete-filiere',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.css']
})
export class DeleteComponent implements OnInit {

  filiere: Filiere = {
    id: 0,
    libelle: "",
    module: [],
    stagiaires: []
   }

  constructor(private filiereService: FiliereService) { }

  ngOnInit(): void {
  }

  handleSubmit(){
    this.filiereService
    .deleteFiliere(this.filiere)
    .subscribe((res: HttpResponse<Filiere>) => {
      console.log(res);
    }
    );  
  }

}
