import { Component, OnInit } from '@angular/core';
import { Filiere } from 'src/interfaces/filiere.interface';
import { FiliereService } from '../filiere.service';
import { switchMap } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'filiere',
  templateUrl: './filiere.component.html',
  styleUrls: ['./filiere.component.css']
})
export class FiliereComponent implements OnInit {

  filieres: Filiere[] = [];

  filiere: Filiere = {
    id: 0,
    libelle: "",
    module: [],
    stagiaires: []
   }

  urlParam: string | null = '';

  constructor(private filiereService: FiliereService, private route: ActivatedRoute) {
    const source$ = this.filiereService.getFilieresAsync();

    source$.pipe(
      switchMap((filieres: Filiere[]) => filieres)
    ).subscribe((filiere) => {
      this.filieres.push(filiere);
    } );
   }

  ngOnInit(): void {
    this.urlParam = this.route.snapshot.paramMap.get('id');
    
  }

  handleSubmit(){
    this.filiereService
    .getByIdFiliere(this.filiere)
    .subscribe((res: Filiere) => {
      this.filiere = res;
    }
    );  
  }

}
