import { HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Filiere } from 'src/interfaces/filiere.interface';
import { FiliereService } from '../filiere.service';

@Component({
  selector: 'update-filiere',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})
export class UpdateComponent implements OnInit {

  filiere: Filiere = {
    id: 0,
    libelle: "",
    module: [],
    stagiaires: []
   }

  constructor(private filiereService: FiliereService) { }

  ngOnInit(): void {
  }

  handleSubmit(){
    this.filiereService
    .postFiliere(this.filiere)
    .subscribe((res: HttpResponse<Filiere>) => {
      console.log(res);
    }
    );  
  }
}
