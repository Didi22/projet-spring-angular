import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Filiere } from 'src/interfaces/filiere.interface';
import { Module } from 'src/interfaces/module.interface';
import { FiliereService } from '../filiere.service';

@Component({
  selector: 'filiere-card',
  templateUrl: './filiere-card.component.html',
  styleUrls: ['./filiere-card.component.css']
})
export class FiliereCardComponent implements OnInit {

  urlParam: string | null = '';

  filiereSelected: Filiere = {
    id: 0,
    libelle: "",
    module: [],
    stagiaires: []
  }

  modules: Module[] = [];

  constructor(private filiereService: FiliereService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.urlParam = this.route.snapshot.paramMap.get('id');
    this.filiereSelected.id = Number(this.urlParam);

    this.filiereService.getByIdFiliere(this.filiereSelected)
    .subscribe((res: any) => this.filiereSelected = res);

    this.filiereService.getModulesOfFiliere(this.filiereSelected)
    .subscribe((res: any) => {
     this.modules = res;
    }
    
    
    );
    
  }

}
