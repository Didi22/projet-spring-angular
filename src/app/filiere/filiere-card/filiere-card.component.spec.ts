import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FiliereCardComponent } from './filiere-card.component';

describe('FiliereCardComponent', () => {
  let component: FiliereCardComponent;
  let fixture: ComponentFixture<FiliereCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FiliereCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FiliereCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
