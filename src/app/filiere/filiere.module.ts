import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FiliereComponent } from './findAllFiliere/filiere.component';
import { PostFiliereComponent } from './postFiliere/post-filiere/post-filiere.component';
import { FormsModule } from '@angular/forms';
import { UpdateComponent } from './update/update.component';
import { DeleteComponent } from './delete/delete.component';
import { RouterModule, Routes } from '@angular/router';
import { FiliereCardComponent } from './filiere-card/filiere-card.component';

const routes: Routes = [
  {path: 'filiere/affichage', component : FiliereComponent},
  {path: 'filiere/affichage/:id', component : FiliereCardComponent},
  {path: 'filiere/enregistrement', component : PostFiliereComponent},
  {path: 'filiere/modification', component : UpdateComponent},
  {path: 'filiere/suppression', component : DeleteComponent}
];

@NgModule({
  declarations: [
    FiliereComponent,
    PostFiliereComponent,
    UpdateComponent,
    DeleteComponent,
    FiliereCardComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forRoot(routes)
  ],
  exports: [FiliereComponent, PostFiliereComponent, UpdateComponent, DeleteComponent, FiliereCardComponent, RouterModule]
})
export class FiliereModule { }
