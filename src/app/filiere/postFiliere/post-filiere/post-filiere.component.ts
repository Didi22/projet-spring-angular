import { HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Filiere } from 'src/interfaces/filiere.interface';
import { FiliereService } from '../../filiere.service';

@Component({
  selector: 'post-filiere',
  templateUrl: './post-filiere.component.html',
  styleUrls: ['./post-filiere.component.css']
})
export class PostFiliereComponent implements OnInit {

  filiere: Filiere = {
    libelle: "",
    module: [],
    stagiaires: []
   }

  constructor(private filiereService: FiliereService) { }

  ngOnInit(): void {
  }

  handleSubmit(){
    this.filiereService
    .postFiliere(this.filiere)
    .subscribe((res: HttpResponse<Filiere>) => {
      console.log(res);
    }
    );  
  }

}
