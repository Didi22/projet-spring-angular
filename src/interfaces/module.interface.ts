export interface Module{
    id?: number;
    dateDebut?: Date;
    dateFin?: Date;
    libelle: string;
    filiere_id?: number;
    personne_id?: number;
}