export interface Filiere{
    id?: number;
    libelle: string;
    module?: [];
    stagiaires?: [];
}